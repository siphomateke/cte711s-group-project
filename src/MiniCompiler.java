import java.util.Scanner;
import java.util.*;

enum TokenType {
    Keyword,
    OpenBracket,
    CloseBracket,
    LeftBrace,
    RightBrace,
    Identifier,
    Symbol,
    Operator,
    Invalid,
    // For ICR form
    Term,
}
class LanguageToken {
    String value;
    TokenType type;
    LanguageToken[] values;

    LanguageToken(String value, TokenType type, LanguageToken[] values) {
        this.value = value;
        this.type = type;
        this.values = values;
    }

    LanguageToken(String value, TokenType type) {
        this(value, type, new LanguageToken[]{});
    }
}

public class MiniCompiler {
    public static boolean isDelimiter(Character ch) {
        return ch == ' ' || ch == '+' || ch == '-' || ch == '*' ||
                ch == '/' || ch == ',' || ch == ';' || ch == '>' ||
                ch == '<' || ch == '=' || ch == '(' || ch == ')' ||
                ch == '[' || ch == ']' || ch == '{' || ch == '}';
    }

    public static boolean isOperator(Character ch) {
        return ch == '+' || ch == '-' || ch == '*' ||
                ch == '/' || ch == '>' || ch == '<' ||
                ch == '=';
    }

    public static boolean validIdentifier(String str) {
        try {
            Integer.parseInt(str);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean isKeyword(String str) {
        return str.equals("if") || str.equals("else") ||
                str.equals("while") || str.equals("do") ||
                str.equals("break") ||
                str.equals("continue") || str.equals("int") || str.equals("double") || str.equals("float")
                || str.equals("return") || str.equals("case") || str.equals("char")
                || str.equals("sizeof") || str.equals("long") || str.equals("short") || str.equals("typedef")
                || str.equals("switch") || str.equals("unsigned") || str.equals("void") || str.equals("static")
                || str.equals("struct") || str.equals("goto") || str.equals("printf") || str.equals("print")
                || str.equals("scanf");
    }

    public static String convertStringToBinary(String input) {
        StringBuilder result = new StringBuilder();
        char[] chars = input.toCharArray();
        for (char aChar : chars) {
            result.append(
                    String.format("%8s", Integer.toBinaryString(aChar))
                            .replaceAll(" ", "0")
            );
        }
        return result.toString();
    }

    public static boolean syntaxAnalysis(String input, ArrayList<LanguageToken> tokens) {
        boolean derivationDone = false;
        String replacement = "";
        for (var token : tokens) {
            if (token.type == TokenType.Identifier) {
                replacement += "E" + token.value;
                derivationDone = true;
            } else {
                replacement += token.value;
            }
        }
        if (!derivationDone) {
            System.out.printf("Cannot derive invalid expression %s%n", input);
            return false;
        }

        System.out.println("GET A DERIVATION FOR : " + input);
        System.out.println("Using the grammar: E=E/E | E=E*E | E=E+E | E=E-E | E=digit | digit={0,1,2,3,4,5,6,7,8,9}");
        System.out.println(replacement);

        for (var token : tokens) {
            if (token.type == TokenType.Identifier) {
                replacement = replacement.replaceFirst("E" + token.value, "digit" + token.value);
                System.out.println(replacement);
            }
        }

        for (var token : tokens) {
            if (token.type == TokenType.Identifier) {
                replacement = replacement.replaceFirst("digit" + token.value, token.value);
                System.out.println(replacement);
            }
        }
        
        return true;
    }

    public static boolean semanticAnalysis(String input, ArrayList<LanguageToken> tokens) {
        ArrayList<String> errors = new ArrayList<>();
        boolean foundOperator = false;
        for (int i = 0; i < tokens.size(); i++) {
            LanguageToken token = tokens.get(i);
            if (i > 0) {
                LanguageToken prev = tokens.get(i - 1);
                if (prev.type == TokenType.Operator && token.type == TokenType.Operator) {
                    errors.add("Semantic error: Use of two operators together ie */, +*, *-, etc, not permitted");
                }
                if (i < tokens.size() - 1) {
                    LanguageToken next = tokens.get(i - 1);
                    if (prev.type == TokenType.Identifier
                            && token.type == TokenType.Operator
                            && token.value.equals("/")
                            && next.type == TokenType.Identifier
                            && next.value.equals("0")) {
                        errors.add("Semantic error: division by zero ie 6/0, not permitted");
                    }

                    if (token.type == TokenType.Operator
                            && (prev.type != TokenType.Identifier
                            || next.type != TokenType.Identifier)) {
                        errors.add("Semantic error: operators can only be next ot identifiers");
                    }
                }
            }
            if (i == tokens.size() - 1) {
                if (!(token.type == TokenType.Symbol && token.value.equals(";"))) {
                    errors.add("Syntax error: Every line must end with a semicolon (;)");
                }
            }
            if (token.type == TokenType.Invalid) {
                errors.add(String.format("Syntax error: Unknown character used: '%s'. Only single numbers [0-9] and operators [/,*,+,-] are allowed.", token.value));
            }
            if (token.type == TokenType.Operator) {
                foundOperator = true;
            }
        }
        if (tokens.size() < 3) {
            errors.add("Semantic error: Input string should be more that 2 characters!");
        }
        if (!foundOperator) {
            errors.add("Semantic error: There is no operator in the string [+,/,-,*]");
        }
        if (errors.size() > 0) {
            for (var error : errors) {
                System.out.printf("- %s%n", error);
            }
            System.out.println("CONCLUSION --> This expression is not syntactically and semantically correct");
            return false;
        } else {
            System.out.println("CONCLUSION --> This expression is syntactically and semantically correct");
            return true;
        }
    }

    public static ArrayList<LanguageToken> extractTerms(String input, ArrayList<LanguageToken> tokens) {
        String[] operatorOrder = {"/", "*", "+", "-"};
        ArrayList<LanguageToken> terms = new ArrayList<>();
        for (var operator : operatorOrder) {
            boolean foundOperator;
            do {
                foundOperator = false;
                ArrayList<Integer> tokenIndicesToRemove = new ArrayList<>();
                for (int j = 0; j < tokens.size(); j++) {
                    var token = tokens.get(j);
                    if (token.type == TokenType.Operator && token.value.equals(operator)) {
                        LanguageToken prevToken = tokens.get(j - 1);
                        LanguageToken nextToken = tokens.get(j + 1);
                        LanguageToken term = new LanguageToken(String.format("t%d", terms.size() + 1), TokenType.Term, new LanguageToken[]{prevToken, token, nextToken});
                        tokens.set(j, term);
                        tokenIndicesToRemove.add(j - 1);
                        tokenIndicesToRemove.add(j + 1);
                        terms.add(term);
                        foundOperator = true;
                        // Break early so that the token indices can be removed before evaluating the next operator
                        break;
                    }
                }
                Collections.reverse(tokenIndicesToRemove);
                for (int index : tokenIndicesToRemove) {
                    tokens.remove(index);
                }
            } while (foundOperator);
        }
        return terms;
    }

    public static int calculateResult(ArrayList<LanguageToken> terms) {
        int result = 0;
        HashMap<String, Integer> variables = new HashMap<>();
        for (var term : terms) {
            int v1;
            if (term.values[0].type == TokenType.Term) {
                v1 = variables.get(term.values[0].value);
            } else {
                v1 = Integer.parseInt(term.values[0].value);
            }
            int v2;
            if (term.values[2].type == TokenType.Term) {
                v2 = variables.get(term.values[2].value);
            } else {
                v2 = Integer.parseInt(term.values[2].value);
            }
            String operator = term.values[1].value;
            int value;
            if (operator.equals("/")) {
                value = v1 / v2;
            } else if (operator.equals("*")) {
                value = v1 * v2;
            } else if (operator.equals("+")) {
                value = v1 + v2;
            } else if (operator.equals("-")) {
                value = v1 - v2;
            } else {
                break;
            }
            variables.put(term.value,  value);
            result = value;
        }
        return result;
    }

    public static void main(String[] args) {
        String open = "(", close = ")", left = "{", right = "}";
        HashMap<String, String> assemblyOperators = new HashMap<>();
        assemblyOperators.put("/", "DIV");
        assemblyOperators.put("*", "MUL");
        assemblyOperators.put("+", "ADD");
        assemblyOperators.put("-", "SUB");

        Scanner scanner = new Scanner(System.in);

        ArrayList<ArrayList<LanguageToken>> allTokens = new ArrayList<>();

        System.out.print("Number of inputs: ");
        int numInputs = scanner.nextInt(); scanner.nextLine();

        String[] inputs = new String[numInputs];
        for (int i = 0; i < numInputs; i++) {
            System.out.println("Enter String (Containing 0 to 9 and/or operators: +,/,*,-):");
            inputs[i] = scanner.nextLine();
        }

        for (int i = 0; i < numInputs; i++) {
            StringTokenizer tokenizer = new StringTokenizer(inputs[i], " ");
            int tokenCount = tokenizer.countTokens();
            ArrayList<LanguageToken> tokens = new ArrayList<>();
            while (tokenizer.hasMoreTokens()) {
                String token = (String) tokenizer.nextElement();
                TokenType tokenType;
                if (isKeyword(token)) {
                    tokenType = TokenType.Keyword;
                } else if (validIdentifier(token)) {
                    tokenType = TokenType.Identifier;
                } else if (isOperator(token.charAt(0))) {
                    tokenType = TokenType.Operator;
                } else if (token.equals(open)) {
                    tokenType = TokenType.OpenBracket;
                } else if (token.equals(close)) {
                    tokenType = TokenType.CloseBracket;
                } else if (token.equals(left)) {
                    tokenType = TokenType.LeftBrace;
                } else if (token.equals(right)) {
                    tokenType = TokenType.RightBrace;
                } else if (isDelimiter(token.charAt(0))) {
                    tokenType = TokenType.Symbol;
                } else {
                    tokenType = TokenType.Invalid;
                }
                tokens.add(new LanguageToken(token, tokenType));
            }
            allTokens.add(tokens);
        }

        for (int i = 0; i < numInputs; i++) {
            ArrayList<LanguageToken> tokens = allTokens.get(i);
            System.out.println("\n====== String #" + (i + 1) + " ======");
            System.out.println("\n======STAGE1: LEXICAL ANALYSIS");
            System.out.println("Symbol table:");
            for (int j = 0; j < tokens.size(); j++) {
                var token = tokens.get(j);
                System.out.println("Token #" + j + " : " + token.value + " (" + token.type + ")");
            }
            System.out.println("Total number of tokens: " + tokens.size());

            System.out.println("\n======STAGE2: SYNTAX ANALYSIS");
            boolean derivationValid = syntaxAnalysis(inputs[i], tokens);
            if (!derivationValid) {
                continue;
            }

            System.out.println("\n======STAGE3: SEMANTIC ANALYSIS");
            boolean valid = semanticAnalysis(inputs[i], tokens);
            if (!valid) {
                continue;
            }

            var terms = extractTerms(inputs[i], tokens);

            System.out.println("\n======STAGE4: INTERMEDIATE CODE REPRESENTATION (ICR)");
            System.out.println("THE STRING ENTERED IS : " + inputs[i]);
            System.out.println("The ICR is as follows:");
            for (LanguageToken term : terms) {
                System.out.printf("%s = %s %s %s%n", term.value, term.values[0].value, term.values[1].value, term.values[2].value);
            }
            System.out.println("CONCLUSION --> The expression was correctly generated in ICR");

            System.out.println("\n======STAGE5: CODE GENERATION");
            for (var term : terms) {
                System.out.printf("LDA %s%n", term.values[0].value);
                System.out.printf("%s %s%n", assemblyOperators.get(term.values[1].value), term.values[2].value);
                System.out.printf("STR %s%n", term.value);
            }

            System.out.println("\n======STAGE6: CODE OPTIMISATION");
            for (var term : terms) {
                System.out.printf("%s %s, %s, %s%n", assemblyOperators.get(term.values[1].value), term.value, term.values[0].value, term.values[2].value);
            }

            System.out.println("\n======STAGE7: TARGET MACHINE CODE");
            for (var term : terms) {
                System.out.printf("%s %s%n", convertStringToBinary(term.values[1].value), Integer.toBinaryString(Integer.parseInt(term.value.replaceAll("t", ""))));
            }

            System.out.println("======END OF COMPILATION");

            int result = calculateResult(terms);
            System.out.printf("%nArithmetic result = %d%n", result);
        }
    }
}
